<?php

namespace App\Http\Controllers;

use App\Models\InvoiceLines;
use App\Models\Invoices;
use App\Models\Relations;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function importClients()
    {
        $mdlClients = new Relations();
        $mdlClients->importDigitaleFactuur();

        $importType = 'Relaties Digitale Factuur';

        return view('import', compact('importType'));
    }

    public function index()
    {
        return view('index');
    }

    public function importInvoices()
    {
        $importType = 'Facturen Digitale Factuur';

        $mdlInvoices = new Invoices();
        $mdlInvoices->importDigitaleFactuur();

        $mdlInvoiceLines = new InvoiceLines();
        $mdlInvoiceLines->importDigitaleFactuur();

        return view('import', compact('importType'));
    }



}
