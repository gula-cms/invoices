<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClientsFinancialInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations', function (Blueprint $table) {
            $table->string('iban');
            $table->string('bic');
            $table->string('kvk_number');
            $table->string('vat_number');
            $table->string('sepa_accountname');
            $table->string('sepa_mandate_reference');
            $table->date('sepa_mandate_date');
            $table->tinyInteger('sepa_business')->default('1');
            $table->tinyInteger('sepa_auto_incasso')->default('0');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
