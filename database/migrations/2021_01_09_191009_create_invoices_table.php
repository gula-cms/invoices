<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->index('idx_user');
            $table->string('invoice_number')->unique()->index('idx_invoice_number');
            $table->integer('id_customer')->index('idx_customer');
            $table->float('amount')->index('idx_amount');
            $table->string('title');
            $table->string('reference');
            $table->dateTime('sent');
            $table->date('expire');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
