<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Gula Factureren</h3>
            <strong>Fa</strong>
        </div>

        <ul class="list-unstyled components">
            <li>
                <a href="/invoices">
                    <i class="fas fa-briefcase"></i>
                    Facturen
                </a>
            </li>
            <li>
                <a href="/expenses">
                    <i class="fas fa-briefcase"></i>
                    Uitgaven
                </a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Contacten
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="/clients">Relaties</a>
                    </li>
                    <li>
                        <a href="/client_contacts">Contactpersonen</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Import / Export
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="/export_invoices">Exporteren facturen</a>
                    </li>
                    <li>
                        <a href="/import_invoices">Importeren facturen</a>
                    </li>
                    <li>
                        <a href="/import_bank">Importeren bank</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/payments">
                    <i class="fas fa-image"></i>
                    Betalingen
                </a>
            </li>
            <li>
                <a href="/vat">
                    <i class="fas fa-question"></i>
                    BTW
                </a>
            </li>
            <li class="dropdown-divider"></li>
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li>
                <a href="https://gula.nl" target="_blank"><img
                        src="https://cms.gula.nl/resizer/160x50/cms/logo/gula_logo.png"/></a>
            </li>
        </ul>
    </nav>
@yield('content')
</div>
