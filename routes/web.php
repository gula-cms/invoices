<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/import_clients', 'InvoiceController@importClients');
Route::get('/import_invoices', 'InvoiceController@importInvoices');
Route::get('/home', 'InvoiceController@index');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'InvoiceController@index')->name('home');
